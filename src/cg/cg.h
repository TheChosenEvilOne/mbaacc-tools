#include "pal/pal.h"
#include "misc.h"

struct alignment {
        int x, y;
        int width, height;
        short source_x, source_y;
        short source_image;
        short copy_flag;
};

struct image_header {
        char filename[32];
        int format; // ??
        unsigned int width;
        unsigned int height;
        unsigned int bpp; // why is this an int, French-Bread explain yourself.
        struct rectangle bounds;
        unsigned int align_start;
        unsigned int align_count;
};

struct image {
	struct image_header header;
	char* pixels;
};

struct cg_header {
	char magic[0x10]; // "BMP Cutter3\x00"
	unsigned char has_palette;
	struct palette palettes[8];
	unsigned int page_count;
	unsigned int unk1; // seems to be always 0
	unsigned int m_nalign; // memory size
	unsigned int image_count;
	unsigned int unk2;
	unsigned int unk3[7]; // seems to be always 0s
	int indices[3000]; // why is this 3000...
	unsigned int align_start;
};

