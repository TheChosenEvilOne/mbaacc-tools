#include <stdio.h>
#include <stdlib.h>
#include "lodepng.h"

#include "cg.h"
	
struct palette* global_palette;

void get_pixel_colour(unsigned char* data, int xy, int format, struct alignment* align, struct palette* pal, char* col) {
	switch (format) {
		case 0: // 8-bit indexed
		case 2:
			col[0] = pal->colours[data[xy]][0];
			col[1] = pal->colours[data[xy]][1];
			col[2] = pal->colours[data[xy]][2];
			col[3] = pal->colours[data[xy]][3] ? 255 : 0; // not sure if format 2 has junk in alpha.
			break;
		case 3:
			col[0] = pal->colours[data[xy]][0];
			col[1] = pal->colours[data[xy]][1];
			col[2] = pal->colours[data[xy]][2];
			col[3] = pal->colours[data[xy]][3];
			break;
		case 4:
			col[0] = pal->colours[data[xy]][0];
			col[1] = pal->colours[data[xy]][1];
			col[2] = pal->colours[data[xy]][2];
			col[3] = data[xy + align->width * align->height];
			break;
		case 1: // BGRA -> RGBA
			col[0] = data[xy * 4 + 2];
			col[1] = data[xy * 4 + 1];
			col[2] = data[xy * 4 + 0];
			col[3] = data[xy * 4 + 3];
			break;
	}
}

struct image_header* read_header(FILE* f, int index, struct cg_header* header, struct image_header* image_header) {
	if (!image_header) // image header can be null.
		image_header = (struct image_header*) malloc(sizeof(struct image_header));
	fseek(f, header->indices[index], SEEK_SET);
	READ_P(struct image_header, f, image_header);
	return image_header;
}

struct image* read_image(FILE *f, struct cg_header* header, struct palette* pal, int index) {
	struct image* image = (struct image*) malloc(sizeof(struct image));
	struct palette* p = NULL;
	read_header(f, index, header, &image->header);
	struct image_header* ih = &image->header;

	char* pixels = (char*) calloc(ih->width * ih->height, 4);
	image->pixels = pixels;
	long address = header->indices[index] + sizeof(struct image_header);
	int bpp = 0; // bytes per pixel, not to be confused with bits per pixel.
	switch (ih->format) { // image bpp is incorrect for extracting.
		case 0: bpp = 1; break; // 8-bit indexed.
		case 1: bpp = 4; break; // 32-bit BGRA.
		case 2: // 8-bit indexed, uses own palette.
		case 4: // 8-bit indexed with 8-bit alpha.
			bpp = ih->format == 2 ? 1 : 2;
			fseek(f, address, SEEK_SET);
			struct palette p2;
			READ_P(struct palette, f, &p2);
			p = &p2;
			address += sizeof(struct palette);
			break;
		case 3: // 8-bit indexed, coloured greyscale.
			bpp = 1;
			fseek(f, address, SEEK_SET);
			unsigned char c[4];
			fread(c, 1, 4, f);
			address += sizeof(c);
			struct palette p3;
			for (int i = 0; i < 256; i++) {
				p3.colours[i][0] = c[0] * i;
				p3.colours[i][1] = c[1] * i;
				p3.colours[i][2] = c[2] * i;
				p3.colours[i][3] = c[3] * i;
			}
			p = &p3;
			break;
	}
	if (!bpp) {
		printf("Error: Unsupported format %d in image %s.\n", ih->format, ih->filename);
		free(image);
		return NULL;
	}
	if (p == NULL) p = pal;
		
	for (unsigned int aind = 0; aind < ih->align_count; aind++) {
		fseek(f, header->align_start + sizeof(struct alignment) * (aind + ih->align_start), SEEK_SET);
		struct alignment align = READ(struct alignment, f);
		if (align.copy_flag)
			continue; // TODO: figure out if there is a way to handle copy flag without building pages.
		int w = align.width >> 4;
		int h = align.height >> 4;
		int x = align.source_x >> 4;
		int y = align.source_y >> 4;
		if (x + w >= 0x10) w = 0x10 - x;
		if (y + h >= 0x10) h = 0x10 - y;
		char* rdata = (char *) calloc(align.width * align.height, bpp);
		fseek(f, address, SEEK_SET);
		fread(rdata, bpp, align.width * align.height, f);

		for (int a = 0; a < h; a++) {
			for (int b = 0; b < w; b++) {
				// doing format 4 like this isn't optimal but the format gave me enough headaches already.
				long offset = ((b * 0x10) + (a * align.width * 0x10)) * (ih->format == 4 ? 1 : bpp);
				for (int c = 0; c < 16 * 16; c++) {
					unsigned char col[4];
					get_pixel_colour(rdata + offset, c, ih->format, &align, p, col);
					int pof = ((b * 16 + align.x + (c % 16)) * 4
						 + (a * 16 + align.y + (c / 16)) * ih->width * 4);
					pixels[pof + 0] = col[0];
					pixels[pof + 1] = col[1];
					pixels[pof + 2] = col[2];
					pixels[pof + 3] = col[3];
				}
			}
		}

		address += align.width * align.height * bpp;
		free(rdata);
	}
	return image;
}

void load_cg(char* path, struct palette* pal) {
	FILE* f = fopen(path, "r");
	struct cg_header header = READ(struct cg_header, f);
	if (!pal)
		pal = &header.palettes[0];

	for (int i = 0; i < header.image_count; i++) {
		struct image* img = read_image(f, &header, pal, i);
		if (!img)
			continue;
		lodepng_encode32_file(img->header.filename, img->pixels, img->header.width, img->header.height);
		free(img->pixels);
		free(img);
	}
}


int main(int argc, char** argv) {
	struct palette* pal = NULL;
	if (argc > 2)
		pal = load_pal(argv[2], 0);
	load_cg(argv[1], pal);
	if (pal)
		free(pal);
}
