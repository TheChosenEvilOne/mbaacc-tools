#pragma once

#define READ(type, file)					\
	({ type v; fread(&v, sizeof(v), 1, file); v;})
#define READ_P(type, file, pointer)				\
	({ fread(pointer, sizeof(type), 1, file); pointer;})

struct rectangle {
	int x1, y1;
	int x2, y2;	
};
