#pragma once

struct palette {
	unsigned char colours[256][4];
};

struct palette* load_pal(char* filename, short id);
