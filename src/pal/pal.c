#include <stdio.h>
#include <stdlib.h>

#include "misc.h"
#include "pal.h"

struct palette* load_pal(char* filename, short id) {
	FILE* f = fopen(filename, "r");
	unsigned int palette_count = READ(unsigned int, f);
	if (id > palette_count) { // the files actually have a lot more palettes, but this is how many are actually used.
		fprintf(stderr, "ERR: Requested palette id %d from file %s while file only has %d palettes.", id, filename, palette_count);
		return NULL;
	}
	struct palette* p = malloc(sizeof(struct palette));
	fseek(f, id * sizeof(struct palette), SEEK_CUR);
	READ_P(struct palette, f, p);
	fclose(f);
	return p;
}

void save_pal(char* filename, short id, struct palette* palette) {
	puts("To Be Implemented...");
}
