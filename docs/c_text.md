# Character INI
The *_c.txt INI files contain all "commands" that can be executed and other changes to the character's stats.

## Command list
The file starts with a list of every "command" usable by the character with each line being a move, the command list ends at the ASCII string "END" in the document.
The following format is used with each field being separated by a whitespace (in this fields will be separated by a comma to make it more readable):

`ID, command, cancel, cancel2, animation, gauge, tobi?, check?`

### Command
Command is an input in numpad notation where neutral is 0:
```
7 8 9
4 0 6
1 2 3
```

#### Cancel
Cancel is a string of flags where each digit is a flag.
The flags are from left to right:
 - Is the command special? 0 - No, 1 - Special, 2 - EX.
 - Is the command guard cancellable? 0 - No, 1 - Yes.
 - Is the command an additional input command? 0 - No, 1 - Yes.
 - Does the command reset the input buffer? 0 - No, 1 - Yes.
 - Is the command cancellable? 0 - Yes, 1 - No.
 - Usable from crouch? 0 - No, 1 - Yes.
 - Usable in air? 0 - No, 1 - Yes.
 - Usable on ground? 0 - No, 1 - Yes.

#### Cancel 2
Cancel 2 is a string of flags where each digit is a flag.
The flags are from left to right:
 - Unused.
 - Simultaneous presses (?)
 - Is shield command? 0 - No, 1 - Yes.
 - Only executable from guard cancel? 0 - No, 1 - Yes.
 - Cancellable in same animation? 0 - No, 1 - Yes.
 - Can be executed before match start? 0 - No, 1 - Yes.
 - ? (受付シビアに)
 - Activated even if button released? 0 - No, 1 - Yes.

#### Animation
The animation field is the ID of the animation (also known as pattern) executed when the command is executed.
See HA6 for more information about animations.

#### Gauge
How much gauge (magic circuit in-game) is used to execute this move?

#### Tobi
Unknown purpose, requires research.

#### Check
Three separate fields. Unknown purpose, requires research.

## Character stats
The stats of the character are in a normal INI format.

The following sections appear in the files:
### TeamChangeData
Each character seems to have this INI section, seems to only have a single property called "test".
Function of the "test" property is unknown, though it does appear to be unused as there is no related string in the executable.

### ThrowParam
This section contains list of animations used for air and ground throws in the "Air" and "Ground" properties.
Both lists appear to terminated with 0.

### ShieldCounter
This section contains animations used for air, crouch, and ground shield counters in the "Air", "Crouch", and "Ground" properties.

### Status
This section contains multiple different properties:

#### CancelDash_Ground
Unknown purpose, requires research.

#### CancelDash_Air
Unknown purpose, requires research.

#### CancelSparkPat_Ground
Unknown purpose, requires research.

#### CancelSparkPat_Air
Unknown purpose, requires research.

#### CancelHighJumpPat
Unknown purpose, requires research.

#### AirJumpNum
How many air jumps the character can do.
This is an 8-bit number.

#### DestroyNum
Unknown purpose, requires research.
This is an 8-bit number.

#### CharaGravity_AddY
Downwards acceleration of the character in air.
This is a 32-bit number which defaults to 150.

#### CharaGravity_MaxX
Unknown purpose, I assume it is a typo of MaxY.
This is a 32-bit number which defaults to 1000.

#### SPCritical
Unknown purpose, only Full-Moon Shiki (shiki_1_c.txt) has a reference to this.
This is an 8-bit number, likely a boolean.

#### TagHeart
Unknown purpose, likely related to switching characters as Hisui&Kohaku.
This is an 8-bit number.

#### KoAniEff
Seems to be an animation that is played as an effect when kicked out, only used by p_arc (Hime).

#### Guard
Unknown purpose, seems to be a list of four floating point values.

#### Flags
Likely related to Sion's bullets (value 1) and Roa's 22 lightning (value 2).
This is an 8-bit number.

#### KoRareVoice
Seems to be a rare voiceline used when KO'd.

#### ExComCheck
Extended Command Check, this section is only used on Ryougi.
This section starts with the "Num" property which is the number of extra checks.
Each extra check has the following properties:
 - "«number»_CheckNum" - Seems to be the command ID
 - "«number»_Type" - Seems to be the type of check
 - "«number»_P0" - Parameter 0
 - "«number»_P1" - Parameter 1
 - "«number»_P2" - Parameter 2
 - "«number»_P3" - Parameter 3
 - "«number»_P4" - Parameter 4
The number is always three digits long (%03d in C string formatting).
Functionality of check types are unknown, only 1 is used.
