# Documentation
These documents contain information about the file formats and other related technical information related to the Melty Blood Actress Again Current Code (MBAACC) engine.
Note that this information is based on the game version used by the "Community Edition" of the game, and likely doesn't match the formats used by the Steam release version.
