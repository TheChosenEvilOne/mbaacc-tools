# cg
The cg file contains images cut into tiles which are stored in "pages" of tiles.
Each tile is 16x16 pixels and a single page can have 64 tiles (meaning each tile is 256x256 pixels).

## Format
The format starts with a sixteen character null terminated ASCII magic string "BMP Cutter3".

[magic string]
[header]
[image headers * header.image_count]

### Header

```
struct header {
	uint8 has_palettes; // boolean value that seems to always be 1, assumed to be palette related.
	uint8 palettes[8][256][4]; // eight 256 colour palettes, seems to always be junk and is never used by the game.
	uint32 page_count; // count of tile pages.
	uint32 unknown1; // unknown purpose, seems to always be 0.
	uint32 align_size; // alignment section size.
	uint32 image_count; // count of images.
	uint32 unknown2; // unknown purpose.
	uint32 unknown3; // unknown purpose.
	uint32 unknown4; // unknown purpose.
	uint32 unknown5; // unknown purpose.
	uint32 unknown6; // unknown purpose.
	uint32 unknown7; // unknown purpose.
	uint32 unknown8; // unknown purpose.
	uint32 unknown9; // unknown purpose.
	int32 image_header_offset[3000]; // Offsets to image headers from start of file.
	uint32 align_start; // 
}
```

